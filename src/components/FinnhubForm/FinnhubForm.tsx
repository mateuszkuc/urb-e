import React, { ChangeEventHandler } from 'react'
import { Form } from "react-bootstrap"

interface Symbol {
    value: string
    label: string
}

interface FinnhubFormProps {
    symbols: Symbol[]
    onQuoteChange: ChangeEventHandler<HTMLSelectElement>
}

const FinnhubForm = ({ symbols, onQuoteChange }: FinnhubFormProps) => {
    return (
        <Form>
            <Form.Label>Symbols</Form.Label>
            <Form.Select onChange={onQuoteChange}>
                {symbols.map(
                    symbol => <option key={symbol.value} value={symbol.value}>{symbol.label}</option>
                )}
            </Form.Select>
        </Form>
    )
}

export default FinnhubForm
