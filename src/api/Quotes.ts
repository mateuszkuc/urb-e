
export const fetchQuotes = (symbol: string) => fetch(
    process.env.REACT_APP_FINNHUB_API_URL + `/stock/quote?symbol=${symbol}&token=` + process.env.REACT_APP_FINNHUB_API_KEY
).then(response => response.json())
