
export const fetchSymbols = (exchange: string) => fetch(
    process.env.REACT_APP_FINNHUB_API_URL + `/stock/symbol?exchange=${exchange}&token=` + process.env.REACT_APP_FINNHUB_API_KEY
).then(response => response.json())
