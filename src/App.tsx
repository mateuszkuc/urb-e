import React, { useState } from 'react';
import { Alert, Row, Col, Container } from "react-bootstrap"
import { FinnhubForm } from "./components"
import { fetchSymbols } from "./api/Symbols"
import { fetchQuotes } from "./api/Quotes"
import useSWR from 'swr'
import { Button, Form, Spinner } from "react-bootstrap"
import get from "lodash/get"

import 'bootstrap/dist/css/bootstrap.css';

interface Symbol {
    description: string, symbol: string
}

function App() {
    const [selectedSymbol, setSelectedSymbol] = useState("")
    const [quotes, setQuotes] = useState(null)
    const [quoteError, setQuoteError] = useState(null)


    const { data: symbols, error } = useSWR('US', fetchSymbols) as { data: Symbol[] , error: any } 


    const onQuoteChange = (event) => {
        setSelectedSymbol(event.target.value)
    }

    console.log(quotes)

    return (
        <Container>
            <Row>   
                <Col>
                    <Alert variant="success">
                        Finnhub
                    </Alert>
                </Col>
            </Row>
            { symbols && !error ? (
                <Row>   
                    <Col>
                        <FinnhubForm
                            symbols={symbols.map((symbol: Symbol) => ({ value: symbol.symbol, label: symbol.description }))}
                            onQuoteChange={onQuoteChange}
                        />
                    </Col>
                </Row>
            ): <Spinner animation="grow"/>}
            <Row>   
                <Col>
                    <Form.Label>Current price</Form.Label>
                    <div>{quotes && !quoteError ? get(quotes, "c") : "There isn't any price for given symbol"}</div>
                </Col>
            </Row>
            <Row>   
                <Col>
                    <Button onClick={() => fetchQuotes(selectedSymbol).then(data => setQuotes(data)).catch(error => setQuoteError(error))}>Get quotes</Button>
                </Col>
            </Row>
        </Container>
    );
}

export default App;
